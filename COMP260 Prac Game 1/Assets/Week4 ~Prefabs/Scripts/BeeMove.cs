﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Week4 
{
	public class BeeMove : MonoBehaviour
	{
		public float speed = 4.0f;        // metres per second
		public float turnSpeed = 180.0f;  // degrees per second
		public Transform player1,player2;
		public Transform Target;
		private Vector2 heading = Vector2.right;



		void Update()
		{
			float distance1 = Vector2.Distance(player1.position, transform.position);
			float distance2 = Vector2.Distance(player2.position, transform.position);
			if (distance1<distance2) 
			{
				Target = player1;
			} 
			else 
			{
				Target = player2;
			}
			// get the vector from the bee to the target 

			Vector2 direction = Target.position - transform.position;




			// calculate how much to turn per frame
			float angle = turnSpeed * Time.deltaTime;

			// turn left or right
			if (direction.IsOnLeft(heading))
			{
				// target on left, rotate anticlockwise
				heading = heading.Rotate(angle);
			}
			else
			{
				// target on right, rotate clockwise
				heading = heading.Rotate(-angle);
			}

			transform.Translate(heading * speed * Time.deltaTime);
		}
		void OnDrawGizmos()
		{
			// draw heading vector in red
			Gizmos.color = Color.red;
			Gizmos.DrawRay(transform.position, heading);

			// draw target vector in yellow
			Gizmos.color = Color.yellow;
			Vector2 direction = Target.position - transform.position;
			Gizmos.DrawRay(transform.position, direction);
		}

	}

}
