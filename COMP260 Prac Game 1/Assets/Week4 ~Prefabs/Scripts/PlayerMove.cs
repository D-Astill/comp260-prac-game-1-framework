﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Week4 
{
public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public string VerticalAxis;
	public string HorizontalAxis;

	void Update() 
	{
		// get the input values
		Vector2 direction;

		direction.x = Input.GetAxis (HorizontalAxis);
		direction.y = Input.GetAxis (VerticalAxis);
		

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}


}
}
