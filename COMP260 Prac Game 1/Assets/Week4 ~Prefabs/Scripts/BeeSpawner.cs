﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Week4
{
public class BeeSpawner : MonoBehaviour 
	{

		public BeeMove beeprefab;
		public int nBees = 50;
		public Rect spawnRect;
		

		void Start () {
			for (int i = 0; i < nBees; i++) {
				// instantiate a bee
				BeeMove bee = Instantiate(beeprefab);

			}
			//new Target = beeprefab.Target;
			PlayerMove player = FindObjectOfType<PlayerMove>();
			//Target = player.transform;

		}

		void OnDrawGizmos()
		{
			Gizmos.color = Color.green;
			Gizmos.DrawLine
			(new Vector2(spawnRect.xMin,spawnRect.yMin),
				new Vector2(spawnRect.xMax,spawnRect.yMin));
			Gizmos.DrawLine
			(new Vector2(spawnRect.xMax,spawnRect.yMin),
				new Vector2(spawnRect.xMax,spawnRect.yMax));
			Gizmos.DrawLine
			(new Vector2(spawnRect.xMax,spawnRect.yMax),
				new Vector2(spawnRect.xMin,spawnRect.yMax));
			Gizmos.DrawLine
			(new Vector2(spawnRect.xMin,spawnRect.yMax),
				new Vector2(spawnRect.xMin,spawnRect.yMin));
		}

			
	}
}
